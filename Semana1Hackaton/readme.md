[![https://cdn.computerhoy.com/sites/navi.axelspringer.es/public/styles/1200/public/media/image/2020/02/lenguaje-programacion-1859691.jpg?itok=m--F6UIs](https://cdn.computerhoy.com/sites/navi.axelspringer.es/public/styles/1200/public/media/image/2020/02/lenguaje-programacion-1859691.jpg?itok=m--F6UIshttps://cdn.computerhoy.com/sites/navi.axelspringer.es/public/styles/1200/public/media/image/2020/02/lenguaje-programacion-1859691.jpg?itok=m--F6UIs "https://cdn.computerhoy.com/sites/navi.axelspringer.es/public/styles/1200/public/media/image/2020/02/lenguaje-programacion-1859691.jpg?itok=m--F6UIs")](https://cdn.computerhoy.com/sites/navi.axelspringer.es/public/styles/1200/public/media/image/2020/02/lenguaje-programacion-1859691.jpg?itok=m--F6UIs "https://cdn.computerhoy.com/sites/navi.axelspringer.es/public/styles/1200/public/media/image/2020/02/lenguaje-programacion-1859691.jpg?itok=m--F6UIs")

[========]

# Un desarrollador FrontEnd
- El desarrollador front-end es quien trae a la vida el diseño visual y de interfaz que le fue entregado.
## Tecnologías que un desarrollador FrontEnd debe conocer:
- Esta especialización es la que le ayuda a traducir de la forma más fidedigna posible los diseños, o sea la estructura, los márgenes, los colores, las tipografías, los elementos gráficos, las animaciones, la adaptación a las pantallas, etcétera.
- Yendo a aspectos más técnicos, para interpretar y traducir los diseños a código, este perfil utiliza diferentes lenguajes de programación:
**1. HTML (hyperText Markup Language):** utilizado para etiquetar, organizar y crear la estructura de los contenidos
**2. CSS (cascading style sheets):** utilizado para crear las cascadas de estilo y darle formato a los contenidos
**3.JavaScript:** lenguaje orientado a objetos que los desarrolladores front-end utilizan, entre otras cosas, para interactuar con HTML y CSS y trabajar la interacción
### Actividades de un frontEnd

```html
Mantener y optimizar sitios web.
Asegurar accessibilidad de web.
Desarrollo Html, Css, Js
```
